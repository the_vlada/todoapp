CREATE DATABASE `TodoApp`;

  CREATE TABLE `TodoApp`.`Todos` (
    `ID` INT NOT NULL AUTO_INCREMENT,
    `TodoID` CHAR(36) NOT NULL,
    `Title` VARCHAR(250) NOT NULL,
    `Comment` VARCHAR(500) NULL,
    `Status` INT NOT NULL,
    `CreatedAt` DATETIME NOT NULL,
    `DueDate` DATETIME NULL,
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `ID_UNIQUE` (`ID` ASC) VISIBLE,
    UNIQUE INDEX `new_tablecol_UNIQUE` (`TodoID` ASC) VISIBLE);


  INSERT INTO Todos (TODOID,TITLE,COMMENT,STATUS,CREATEDAT, DUEDATE)
  VALUE('e9a99974-a12b-11ea-bb37-0242ac130098', 'New item', 'Comment about item', 0, NOW(), NOW());


