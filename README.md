# TODO App

A simple Todo app was created for learning. It has frontend (HTML, Javascript), backend (Java and Maven with Spark) and database (Docker container with MySQL).

## Prerequisites

Download project
```
    git clone https://the_vlada@bitbucket.org/the_vlada/todoapp.git
```

To build this project next software need be installed.

* MySQL databe or contenerized Docker version
* Apache Maven 3.6.0
* Some IDE for java application, my chose was IntelliJ IDEA with JDK 12

## Environment setup

### Docker

Once Docker is installed on your machine, MySQL container should be configured. To do that the following steps must be done:


1. Donwload MySQL image
```
    docker pull mysql:latest
```

2. Create container
```
    docker run --name [name] -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=[password] useSSL=false mysql:latest 
```

**IMPORTANT!** selected password should be created according to the requirements of MySQL Server. If password does not satisfy rules, user will not be able to interact with MySQL Server. In this example were used username "root" and password "MYSQLp4ssw0rd!".

3. Open BASH session in container
```
    docker exec -it mysql-container /bin/bash
```

4. Run MySQL Server
```
    #mysql -u [username] -p [password]
```

## Database creation
When MySQL Server running, it is possible to create database with schema, run query from this [file](https://bitbucket.org/the_vlada/todoapp/src/master/db_scripts/create_database.sql).

## Running TodoApp
Make sure the container is up
```
    docker container ls
```

If not (fields are empty), than check all containers
```
    docker container ls -a
```

Find [name] container and run
```
    docker start [container id]
```

For IntelliJ IDEA

* Run IntelliJ IDEA. Open project TodoApp. Go to File>Settings>Plugins. Find and download Lombok plugin. Restart IntelliJ IDEA.
* Run IntelliJ IDEA. Open project TodoApp. Go to File>Project structure and check version of SDK(it should be 12 for this project). Apply the changes if necessary
* Find file pom.xml in project, click right button on it, target Maven and choose Reload project.
* Go to Build and choose Build project.
* Go to Run and choose Run TodoApplication.
* Open new page in browser with address http://localhost:9999/.
* Use TodoApp page.

## Running the tests

Check if docker container run.

Run TodoPageSeleniumTest.java

## Built With

* [Docker](https://www.docker.com/) - Service with containers
* [IDE](https://www.jetbrains.com/idea/) - IDE for Java 
* [Maven](https://maven.apache.org/) - Dependency Management

