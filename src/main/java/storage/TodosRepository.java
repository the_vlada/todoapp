package storage;

import model.Todo;

import java.sql.SQLException;
import java.util.Collection;

public interface TodosRepository {
    Collection<Todo> getAll() throws SQLException, ClassNotFoundException;

    String add(Todo todo) throws SQLException, ClassNotFoundException;

    Collection<String> remove(String[] ids) throws SQLException, ClassNotFoundException;

    Collection<String> updateTodos(Todo[] todos) throws SQLException, ClassNotFoundException;
}
