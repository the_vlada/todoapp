package storage;

import model.Status;
import model.Todo;
import org.joda.time.DateTime;

import java.sql.*;
import java.util.*;

public class MySqlRepository implements TodosRepository {

    private final String databaseUrl;
    private final String userName;
    private final String password;
    private Connection connection;

    public MySqlRepository(String databaseUrl, String userName, String password) {
        this.databaseUrl = databaseUrl;
        this.userName = userName;
        this.password = password;
    }

    @Override
    public Collection<Todo> getAll() throws SQLException, ClassNotFoundException {
        String query = "select todoid, title, comment, status, duedate from Todos";

        connect();

        Statement stmt = connection.createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        List<Todo> todos = new ArrayList<>();

        while (resultSet.next()) {
            var id = UUID.fromString(resultSet.getString(1));
            var title = resultSet.getString(2);
            var comment = resultSet.getString(3);
            var status = Status.values()[resultSet.getInt(4)];
            var duedate = DateTime.parse(resultSet.getString(5).replace(" ", "T"));
            var todo = new Todo(id, title, comment, status, duedate);

            todos.add(todo);
        }

        disconnect();
        return todos;
    }

    @Override
    public String add(Todo todo) throws SQLException, ClassNotFoundException {
        connect();

        String query = "INSERT INTO Todos (TODOID,TITLE,COMMENT,STATUS,CREATEDAT, DUEDATE)" +
                "VALUE('" + todo.getId() + "', '" +
                todo.getTitle() + "', '" +
                todo.getComment() + "', " +
                todo.getStatus().ordinal() + ", " +
                "NOW(), '" +
                todo.getDuedate().toString().substring(0, 19) + "');";
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
            return todo.getId();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            disconnect();
        }
        return null;
    }

    @Override
    public Collection<String> remove(String[] ids) throws SQLException, ClassNotFoundException {
        var idsString = String.join(",", Arrays.stream(ids).map(id -> "'" + id + "'")
                .toArray(String[]::new));

        String query = "DELETE FROM Todos WHERE TodoID IN (" + idsString + ")";
        connect();
        Statement stmt = connection.createStatement();
        stmt.executeUpdate(query);

        disconnect();
        return Arrays.asList(ids);
    }

    @Override
    public Collection<String> updateTodos(Todo[] todos) throws SQLException, ClassNotFoundException {
        connect();

        var result = new ArrayList<String>();

        for (var todo : todos) {
            try {
                var query = "UPDATE Todos " +
                        "SET Title='" + todo.getTitle() + "', " +
                        "Comment='" + todo.getComment() + "', " +
                        "Status='" + todo.getStatus().ordinal() + "', " +
                        "Duedate='" + todo.getDuedate().toString().substring(0, 19) + "' " +
                        "WHERE TodoID = '" + todo.getId() + "' ";

                Statement stmt = connection.createStatement();
                stmt.executeUpdate(query);
                result.add(todo.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        disconnect();
        return result;
    }

    private void connect() throws ClassNotFoundException, SQLException {

        if (connection == null || connection.isClosed()) {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(databaseUrl, userName, password);
        }
    }

    private void disconnect() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
