package storage;

import model.Todo;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;

public class InMemoryRepository implements TodosRepository {

    private final HashMap<String, Todo> DATA = new HashMap<>();

    @Override
    public Collection<Todo> getAll() {
        return DATA.values();
    }

    @Override
    public String add(Todo todo) {
        DATA.put(todo.getId(), todo);
        return todo.getId();
    }

    @Override
    public Collection<String> remove(String[] ids) {

        var removedIds = Arrays
                .stream(ids)
                .filter(id -> DATA.getOrDefault(id, null) != null)
                .map(id -> DATA.remove(id).getId())
                .toArray(String[]::new);

        return Arrays.asList(removedIds);
    }

    @Override
    public Collection<String> updateTodos(Todo[] todos) {
        var updated = Arrays.stream(todos)
                .map(todo -> Objects.requireNonNull(DATA.put(todo.getId(), todo)).getId())
                .toArray(String[]::new);

        return Arrays.asList(updated);
    }
}
