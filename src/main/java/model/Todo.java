package model;

import lombok.Data;
import org.joda.time.DateTime;

import java.util.UUID;

@Data
public class Todo {
    final String id;
    String title;
    String comment;
    Status status;
    DateTime duedate;

    public Todo(UUID id, String title, String comment, Status status, DateTime duedate) {
        this.id = id.toString();
        this.title = title;
        this.comment = comment;
        this.status = status;
        this.duedate = duedate;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public DateTime getDuedate() {
        return duedate;
    }

    public void setDuedate(DateTime duedate) {
        this.duedate = duedate;
    }
}
