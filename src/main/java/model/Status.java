package model;

public enum Status {
    ACTIVE,
    COMPLETED,
    PLANNED
}