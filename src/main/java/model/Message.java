package model;

public class Message<T> {
    private final T payload;
    private final Exception error;
    private final boolean isSuccess;

    public T getPayload() {
        return payload;
    }

    public Exception getError() {
        return error;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public Message(T payload){
        this.isSuccess = true;
        this.payload = payload;
        this.error = null;
    }

    public Message(Exception error){
        this.isSuccess = false;
        this.error = error;
        this.payload = null;
    }
}
