package routing;

import static spark.Spark.*;

public class Routing {

    public Routing(int port, String indexPage, Presenter presenter) {
        port(port);

        staticFiles.location("/public");
        get("/", (request, response) -> indexPage);

        get("/todos", (request, response) -> presenter.getAllTodos());

        // on create
        post("/todos", (request, response) -> presenter.createTodos(request));

        delete("/todos", (request, response) -> presenter.Delete(request));

        // on update
        put("/todos", (request, response) -> presenter.Update(request));
    }
}
