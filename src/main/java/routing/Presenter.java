package routing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Message;
import model.Status;
import model.Todo;
import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import org.javatuples.KeyValue;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import spark.Request;
import storage.TodosRepository;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Stream;

public class Presenter {
    private final TodosRepository todosRepository;
    private final ObjectMapper mapper;

    public Presenter(TodosRepository todosRepository, ObjectMapper mapper) {
        this.todosRepository = todosRepository;

        this.mapper = mapper;
    }

    public String getAllTodos() throws JsonProcessingException {

        Message<Object[]> result;
        try {
            result = new Message<>(todosRepository.getAll()
                    .stream()
                    .map(todo -> new Object() {
                        public final String id = todo.getId();
                        public final String title = todo.getTitle();
                        public final String comment = todo.getComment();
                        public final Status status = todo.getStatus();
                        public final String duedate = todo.getDuedate().toString();
                    }).toArray());
        } catch (Exception e) {
            System.out.println(e);
            result = new Message<>(new Exception("Error while get all todos", e));
        }

        return mapper.writeValueAsString(result);
    }

    public String createTodos(Request request) throws JsonProcessingException {
        Message<Collection<KeyValue<String, String>>> result;

        try {
            var todos = parseBody(request);
            var created = new ArrayList<KeyValue<String, String>>();
            for (var pair : todos) {

                var idCreated = todosRepository.add(pair.getValue());
                created.add(new KeyValue<>(pair.getKey().toString(), idCreated));
            }

            result = new Message<>(created);

        } catch (Exception e) {
            System.out.println(e);
            result = new Message<>(new Exception("Error while adding todos", e));
        }
        return mapper.writeValueAsString(result);
    }

    public String Delete(Request request) throws JsonProcessingException {
        var ids = request.body().split(",");
        Message<Collection<String>> result;
        try {

            result = new Message<>(todosRepository.remove(ids));
        } catch (Exception e) {
            System.out.println(e);
            result = new Message<>(new Exception("Error while adding removing: " + String.join(",", ids), e));
        }

        return mapper.writeValueAsString(result);

    }

    public String Update(Request request) throws JsonProcessingException {
        Message<Collection<String>> result;
        try {
            var todos = parseBody(request).stream()
                    .map(pair -> pair.getValue())
                    .toArray(Todo[]::new);

            var updated = todosRepository.updateTodos(todos);
            result = new Message<>(updated);
        } catch (Exception e) {
            System.out.println(e);
            result = new Message<>(new Exception("Error while adding removing todos", e));
        }
        return mapper.writeValueAsString(result);
    }

    private ArrayList<KeyValue<Integer, Todo>> parseBody(Request request) throws IOException, ParseException {
        var tree = mapper.readTree(request.body());
        var todos = new ArrayList<KeyValue<Integer, Todo>>();

        for (JsonNode child : tree) {
            Integer oldId = null;
            UUID id;
            var rawId = child.get("id").asText();
            try {
                oldId = Integer.parseInt(rawId);
                id = UUID.randomUUID();
            } catch (NumberFormatException e) {
                id = UUID.fromString(rawId);
            }
            var title = child.get("title").asText();
            var comment = child.get("comment").asText();
            var status = Status.valueOf(child.get("status").asText());
            var duedate = DateTime.parse(child.get("duedate").asText()).toDateTime(DateTimeZone.UTC);
            todos.add(new KeyValue(oldId, new Todo(id, title, comment, status, duedate)));
        }
        return todos;
    }
}
