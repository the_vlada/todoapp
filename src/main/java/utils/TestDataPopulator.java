package utils;

import model.Status;
import model.Todo;
import org.joda.time.DateTime;
import storage.TodosRepository;

import java.sql.SQLException;
import java.util.UUID;

public class TestDataPopulator {
    public static void populateTestData(TodosRepository repository) throws SQLException, ClassNotFoundException {
        for (int i = 0; i < 5; i++) {
            repository.add(new Todo(UUID.randomUUID(), "title_" + i, "comment", Status.ACTIVE, DateTime.parse("2020/01/01 10:30:00")));
        }
    }
}
