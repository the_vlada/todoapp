import com.fasterxml.jackson.databind.ObjectMapper;
import routing.Presenter;
import routing.Routing;
import storage.InMemoryRepository;
import storage.MySqlRepository;
import storage.TodosRepository;
import utils.TestDataPopulator;

import java.nio.file.Files;
import java.nio.file.Paths;

public class TodoListApplication {

    public static void main(String[] args) throws Exception {

        TodosRepository storage;

        if (args.length > 0 && "inmemory".equals(args[0])) {
            storage = new InMemoryRepository();
            TestDataPopulator.populateTestData(storage);

        } else {
            storage = new MySqlRepository(
                    "jdbc:mysql://localhost:3306/TodoApp?allowPublicKeyRetrieval=true&useSSL=false",
                    "root",
                    "MYSQLp4ssw0rd!");

            var presenter = new Presenter(storage, new ObjectMapper());
            var indexPage = Files.readString(Paths.get("src/main/resources/public/index.html"));
            new Routing(9999, indexPage, presenter);
        }
    }
}