function send(resource, method, func, body) {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            func(xhttp.responseText);
        }
    };

    xhttp.open(method, resource, true);
    xhttp.send(body);
}

function handleResponse(response, onSuccess, onError) {
    var parsed = JSON.parse(response);
    if (parsed.success) {
        onSuccess(parsed.payload);
    } else {
        onError(parsed.error);
    }
}