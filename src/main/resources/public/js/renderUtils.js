function renderNewTodoItem(todo, list) {
    if (list.style.borderTop === "") {
        list.style.borderTop = "2px solid white";
    }

    return `
    <li id="li-${todo.id}" class="row justify-content-between">
        ${createEditButton(todo.id)}
        <text id="textTitle" class="col-7">${todo.title}</text>
        <text id="textStatus" class="col-auto">${todo.status}</text>
        <input id="box-${todo.id}" name="${todo.id}" class="checkboxes col-1" type="checkbox">    
    </li>`;
}

function createSelectDropdown(id) {
    return `<label for="status">
        </label>
            <select class="col-auto" name="status" id="statusChangeSelect-${id}" title="${id}">
                <option value="ACTIVE">Active</option>
                <option value="COMPLETED">Completed</option>
                <option value="PLANNED">Planned</option>
            </select>`;
}

function createEditButton(id) {

    return `<button type="button"
        class="btn btn-info btn-lg col-auto fas fa-pencil-alt" 
        id="editButton" name="${id}"
        data-toggle="modal"
        data-target="#editModal"
        onclick="createEditForm(event)"></button>`;
}

function createEditForm(event) {

    let todo = todos.get(event.currentTarget.name);

    let editForm = document.getElementById("editForm");

    editForm.innerHTML = `
        <div id="editedTodoId" style="display: none">${todo.id}</div>
        <div class="input-group">
            <i class="fas fa-highlighter icon"></i>
            <input id="titleEdit" value="${todo.title}">
        </div>
        <div class="input-group">
            <i class="far fa-comment-alt icon"></i>
            <input id="commentEdit" value="${todo.comment}">
        </div>
        <div class="input-group">
            <i class="fas fa-angle-double-down icon"></i>
            ${createSelectDropdown(todo.id)}
        </div>
        <div class="input-group">
            <i class="far fa-calendar-alt icon"></i>
            <input type="datetime-local" id="duedateEdit"
                name="duedateEdit" value="${moment(todo.duedate).format("YYYY-MM-DDThh:mm")}">
        </div>`

}


function displayList() {
    list.style.borderTop = "2px solid white";
    list.innerHTML = "";

    todos.forEach(todo => {
        if (todo.localState !== state.DELETED) {
            list.insertAdjacentHTML("beforeend", renderNewTodoItem(todo, list));
            document.getElementById("box-" + todo.id).checked = todo.checked;
        }
    });

    if (todos.length > 0) {
        addListElementsListners();
    }
}
