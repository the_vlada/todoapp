const todosForm = document.getElementById("todos-form");
const input = document.getElementById("input");
const submitButton = document.getElementById("submitButton");
const list = document.getElementById("list");
const allButton = document.getElementById("allButton");
const activeButton = document.getElementById("activeButton");
const completedButton = document.getElementById("completedButton");
const plannedButton = document.getElementById("plannedButton");
const deleteButton = document.getElementById("deleteButton");
const selectButton = document.getElementById("selectButton");
const saveButton = document.getElementById("saveButton");
const editButtton = document.getElementById("editButton");
const editModal = document.getElementById("editModal");

let globalId = 1;

const todos = new Map();

window.onload = function () {

    getAllTodos();

    submitButton.addEventListener("click", addTodoItem); 	//button submit event listener

    allButton.addEventListener("click", () => displayByStatus(_ => true));     //button all event listener

    activeButton.addEventListener("click", () => displayByStatus(status => status === 'ACTIVE'));     //button active event listener

    completedButton.addEventListener("click", () => displayByStatus(status => status === 'COMPLETED'));      //button complete event listener

    plannedButton.addEventListener("click", () => displayByStatus(status => status === 'PLANNED'));     //button planned event listener

    deleteButton.addEventListener("click", deleteSelected);    //button delete selected event listener

    selectButton.addEventListener("click", selectAll);    //button select event listener

    saveButton.addEventListener("click", save);    //event listener for save

    list.addEventListener("click", boxChecked);
}

const status = {
    ACTIVATE: '0',
    COMPLETED: '1',
    PLANNED: '2',
}

const state = {
    UNCHANGED: '0',
    CREATED: '1',
    MODIFIED: '2',
    DELETED: '3',
}

function getAllTodos() {

    send("/todos", "GET", (response) => {

        let onError = (err) => alert(err.message);

        let onSuccess = (payload) => {
            console.log("get all" + JSON.stringify(payload));
            payload.forEach(todo => {

                let utcDate = new Date(todo.duedate);
                let localDate = new Date(utcDate.getTime() - utcDate.getTimezoneOffset() * 60000);

                todos.set(
                    todo.id,
                    {
                        id: todo.id,
                        title: todo.title,
                        comment: todo.comment,
                        duedate: localDate,
                        status: todo.status,
                        checked: false,
                        localState: state.UNCHANGED
                    })
            });

            displayList();
        }

        handleResponse(response, onSuccess, onError);
    });
}

function onEditModalSave(event) {
    let todo = todos.get(document.getElementById("editedTodoId").textContent);


    todo.title = document.getElementById("titleEdit").value;
    todo.duedate = new Date(document.getElementById("duedateEdit").value);
    todo.status = document.getElementById(`statusChangeSelect-${todo.id}`).value.toUpperCase();
    todo.comment = document.getElementById("commentEdit").value;

    console.log("new todo from modal: " + JSON.stringify(todo));

    setLocalState(todo);
    displayList();
}


function setLocalState(todo) {
    if (todo.localState === state.UNCHANGED) {
        todo.localState = state.MODIFIED;
    }
}

function boxChecked(event) {
    const element = event.target;
    if (element.type === "checkbox") {

        let todo = todos.get(element.name);
        if (todo) {
            todo.checked = element.checked;
        }
    }
}

function selectAll() {
    let buttonSelect = document.getElementById("selectButton");

    if (buttonSelect.textContent === "Select All") {
        todos.forEach(todo => todo.checked = true);
        buttonSelect.textContent = "Select None"

    } else {
        todos.forEach(todo => todo.checked = false);
        buttonSelect.textContent = "Select All"
    }
    displayList();
}

function addTodoItem() {
    if (input.value === "") {
        alert("You must enter some value!");
        return;
    }

    let todo = {
        id: `${globalId++}`,
        title: input.value,
        comment: "",
        status: "ACTIVE",
        duedate: new Date(),
        checked: false,
        localState: state.CREATED
    }

    todos.set(todo.id, todo);

    displayList();
}


function displayByStatus(shouldDisplay) {
    let htmlItems = document.getElementById("list").getElementsByTagName("li");
    todos.forEach(todo => {

        let htmlItem = htmlItems.namedItem(`li-${todo.id}`);

        if (htmlItem) {
            htmlItem.style.display = shouldDisplay(todo.status)
                ? ""
                : "none";
        }
    });
}

function deleteSelected() {
    todos.forEach(todo => {
        if (todo.checked) {
            if (todo.id.length < 10) {
                todos.delete(todo.id);
            } else {
                todo.localState = state.DELETED
            }
        };
    });
    todosForm.reset();
    displayList();
}

function save() {

    let created = [];
    let modified = [];
    let toRemove = [];

    todos.forEach(todo => {
        switch (todo.localState) {
            case state.CREATED:
                created.push({ id: todo.id, title: todo.title, comment: todo.comment, status: todo.status, duedate: todo.duedate });
                break;
            case state.MODIFIED:
                modified.push({ id: todo.id, title: todo.title, comment: todo.comment, status: todo.status, duedate: todo.duedate });
                break;
            case state.DELETED:
                toRemove.push(todo.id);
                break;
        }
    });

    if (created.length > 0) {
        update = (response) => {

            let onSuccess = updatedIds => {
                todos.get()
                updatedIds.forEach(pair => {
                    var oldTodo = todos.get(pair.key);
                    todos.delete(pair.key);
                    todos.set(pair.value, { ...oldTodo, id: pair.value, localState: state.UNCHANGED });
                });
            }

            let onError = err => alert(err);

            handleResponse(response, onSuccess, onError);
        }

        let createdJson = JSON.stringify(created)
        console.log("created to save: " + createdJson);


        send('/todos', "POST", update, createdJson);
    }

    if (modified.length > 0) {
        update = (response) => {

            let onSuccess = updatedIds => {
                updatedIds.forEach(id => todos.get(id).localState = state.UNCHANGED);
            }

            let onError = err => alert(err);

            handleResponse(response, onSuccess, onError);
        }

        let modifiedJson = JSON.stringify(modified)
        console.log("modified to save: " + modifiedJson);

        send('/todos', "PUT", update, modifiedJson);
    }

    if (toRemove.length > 0) {
        let update = response => {

            let onSuccess = removedIds => {
                removedIds.forEach(id => todos.delete(id));
                displayList();
            };

            let onError = err => alert(err);

            handleResponse(response, onSuccess, onError);
        };

        send('/todos', "DELETE", update, toRemove);
    }
}
