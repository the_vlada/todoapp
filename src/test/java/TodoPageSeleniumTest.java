import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TodoPageSeleniumTest {
    ChromeDriver driver = new ChromeDriver();

    @BeforeEach
    void setUp() {
        driver.get("http://localhost:9999/");
    }

    @AfterEach
    void tearDown() {
        driver.close();
    }

    @Test
    void createItem_newItemInList() {
        String inputValue = "test selenium";
        WebElement input = driver.findElement(By.id("input"));
        input.sendKeys(inputValue);
        WebElement submitButton = driver.findElement(By.id("submitButton"));
        //WebElement saveButton = driver.findElement(By.id("saveButton"));
        submitButton.click();
        //saveButton.click();
        WebElement newItem = driver.findElement(By.id("li-1"));
        WebElement textInTitle = newItem.findElement(By.id("textInTitle"));
        String result = textInTitle.getText();
        assertEquals(inputValue, result);
    }

    @Test
    void deleteItem_deleteItemFromList() {
        List<WebElement> listBefore = driver.findElements(By.tagName("li"));
        WebElement item = listBefore.get(0);
        WebElement checkbox = item.findElement(By.tagName("input"));
        checkbox.click();
        WebElement deleteButton = driver.findElement(By.id("deleteButton"));
        deleteButton.click();
        int expected = listBefore.size();
        List<WebElement> listAfter = driver.findElements(By.tagName("li"));
        int result = listAfter.size() + 1;
        assertEquals(expected, result);
    }

    @Test
    void editItem_newDataItem() {
        String  expectedTitle = "this is edited title";
        String  expectedStatus = "Completed";
        List<WebElement> listBefore = driver.findElements(By.tagName("li"));
        WebElement item = listBefore.get(0);
        WebElement editButton = item.findElement(By.id("editButton"));
        editButton.click();

        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement title = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("titleEdit")));
        title.clear();
        title.sendKeys(expectedTitle);
        WebElement status = driver.findElement(By.tagName("select"));
        status.click();
        Select dropdown = new Select(status);
        dropdown.selectByVisibleText(expectedStatus);
        WebElement saveButton = driver.findElement(By.cssSelector("#editModal > div > div > div.modal-footer > button"));

        saveButton.click();

        List<WebElement> listAfter = driver.findElements(By.tagName("li"));
        WebElement newItem = listAfter.get(0);
        WebElement newTitle = newItem.findElement(By.id("textTitle"));
        WebElement newStatus = newItem.findElement(By.id("textStatus"));

        String resultTitle = newTitle.getText();
        String resultStatus = newStatus.getText();
        assertEquals(expectedTitle, resultTitle);
        assertEquals(expectedStatus.toUpperCase(), resultStatus);
    }
}
